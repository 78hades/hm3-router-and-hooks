import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  countBasket: 0,
  basket: [],
  isBasket: false,
};

const basketSlice = createSlice({
  name: "basket",
  initialState,
  reducers: {
    fillBasket: (state, action) => {
      state.countBasket += 1;
      state.basket.push(action.payload);
    },
    clearBasket: (state, action) => {
      state.countBasket -= 1;
      const index = state.basket.findIndex(
        (product) => product.id === action.payload.id
      );
      if (index !== -1) {
        state.basket.splice(index, 1);
      }
    },
    isBasketAction: (state, action) => {
      state.isBasket = action.payload;
    },
    lSCountBasket: (state, action) => {
      state.countBasket = action.payload;
    },
    lSbasket: (state, action) => {
      state.basket = action.payload;
    },
  },
});

export const {
  fillBasket,
  clearBasket,
  isBasketAction,
  lSCountBasket,
  lSbasket,
} = basketSlice.actions;

export default basketSlice.reducer;
