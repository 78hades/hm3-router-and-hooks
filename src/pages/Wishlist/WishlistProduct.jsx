import Button from "../../components/Button/Button";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { isShowFirst } from "../../redux/showModal.slice/showModal.slice";
import { getProduct } from "../../redux/product.slice/product.slice";
import { isWishlistAction } from "../../redux/wishlist.slice/wishlist.slice";
import { isShowSecond } from "../../redux/showModal.slice/showModal.slice";
import { isBasketAction } from "../../redux/basket.slice/basket.slice";
function Favoriteproduct({ product }) {
  const dispatch = useDispatch();
  return (
    <>
      <div className="wishlist__product product">
        <Button
          type="button"
          classNames="product__delete"
          onClick={() => {
            dispatch(getProduct(product));
            dispatch(isShowFirst(true));
            dispatch(isWishlistAction(true));
          }}
        ></Button>{" "}
        <img className="product__img" src={product.pathImg} alt="" />{" "}
        <div className="product__about">
          <p className="product__about-name">{product.name}</p>
          <p className="product__about-color"> Color: {product.color}</p>
          <p className="product__about-type"> Type: {product.type}</p>
        </div>
        <p className="product__price">{product.price}</p>
        <Button
          type="button"
          classNames="product__add-to-cart"
          onClick={() => {
            dispatch(getProduct(product));
            dispatch(isShowSecond(true));
            dispatch(isBasketAction(true));
          }}
        >
          Add to cart
        </Button>{" "}
      </div>
    </>
  );
}
Favoriteproduct.propTypes = {
  product: PropTypes.object,
};

export default Favoriteproduct;
