import Button from "../../components/Button/Button";
import deleteImg from "./deletecon.png";
import { isShowFirst } from "../../redux/showModal.slice/showModal.slice";
import { useDispatch } from "react-redux";
import { getProduct } from "../../redux/product.slice/product.slice";
import { isBasketAction } from "../../redux/basket.slice/basket.slice";
export default function Product({ product, quantity, subtotal }) {
  const dispatch = useDispatch();
  return (
    <>
      <div className="cart-page__product product">
        <img className="product__img" src={product.pathImg} alt="" />
        <div className="product__about">
          {" "}
          <p className="product__about-name">{product.name}</p>
          <p className="product__about-color">Color: {product.color} </p>
          <p className="product__about-quantity">Quantity: {quantity} </p>
        </div>
        <p className="product__price">{product.price}</p>
        <p className="product__shipping">Free</p>
        <p className="product__subtotal">${subtotal}</p>
        <Button
          type="button"
          classNames="product__delete"
          onClick={() => {
            dispatch(getProduct(product));
            dispatch(isShowFirst(true));
            dispatch(isBasketAction(true));
          }}
        >
          <img src={deleteImg} alt="" />
        </Button>{" "}
      </div>
    </>
  );
}
