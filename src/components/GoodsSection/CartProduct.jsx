import PropTypes from "prop-types";
import Button from "../Button/Button";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { isShowSecond } from "../../redux/showModal.slice/showModal.slice";
import { getProduct } from "../../redux/product.slice/product.slice";
import { isBasketAction } from "../../redux/basket.slice/basket.slice";
import {
  clearWishlist,
  fillWishlist,
} from "../../redux/wishlist.slice/wishlist.slice";
function CardProduct({ product }) {
  const dispatch = useDispatch();
  const [colorStar, setColorStar] = useState(false);
  const { wishlist } = useSelector((state) => state.wishlist);
  useEffect(() => {
    localStorage.setItem(`star${product.id}`, JSON.stringify(colorStar));
  }, [product.id, colorStar]);

  const [secondClick, setSecondClick] = useState(0);

  const handleClickStar = () => {
    const currTime = new Date().getTime();
    const diffTime = currTime - secondClick;

    if (diffTime < 500) {
      dispatch(clearWishlist(product));
      setColorStar(false);
    } else {
      if (!colorStar) {
        dispatch(fillWishlist(product));
      }
    }

    setSecondClick(currTime);
  };
  useEffect(() => {
    setColorStar(wishlist.some((item) => item.id === product.id));
  }, [wishlist, product]);
  
  return (
    <div className="products__product">
      <img className="products__product-img" src={product.pathImg} alt="" />
      <h3 className="products__product-name">{product.name}</h3>
      <span className="products__product-type">{product.type}</span>
      <span className="products__product-price">{product.price}</span>
      <span className="products__product-color">{product.color}</span>
      <div
        onClick={() => dispatch(isShowSecond(true))}
        className="products__product-add"
      >
        <p className="products__product-plus">+</p>{" "}
        <Button
          type="button"
          classNames="products__product-btn"
          onClick={() => {
            dispatch(getProduct(product));
            dispatch(isShowSecond(true));
            dispatch(isBasketAction(true));
          }}
        >
          Add to cart
        </Button>{" "}
      </div>
      <svg
        className="products__product-favorites"
        onClick={handleClickStar}
        version="1.0"
        xmlns="http://www.w3.org/2000/svg"
        width="30px"
        height="30px"
        viewBox="0 0 1280.000000 1216.000000"
        preserveAspectRatio="xMidYMid meet"
      >
        <metadata>
          Created by potrace 1.15, written by Peter Selinger 2001-2017
        </metadata>
        <g
          transform="translate(0.000000,1216.000000) scale(0.100000,-0.100000)"
          fill={colorStar ? "#da00fe" : "#8A8A8A"}
          stroke="none"
        >
          <path
            d="M5890 10598 c-332 -755 -736 -1674 -898 -2043 -161 -368 -295 -671
-297 -673 -2 -2 -308 -25 -682 -52 -373 -27 -1054 -76 -1513 -109 -459 -34
-1087 -79 -1395 -101 -308 -22 -585 -43 -615 -46 l-54 -6 49 -47 c28 -25 336
-300 684 -611 349 -311 806 -718 1016 -905 1267 -1130 1560 -1391 1572 -1400
17 -13 74 228 -542 -2265 -256 -1036 -464 -1887 -463 -1890 2 -4 869 499 1928
1117 1058 618 1931 1122 1940 1120 8 -2 398 -242 865 -532 468 -291 1165 -724
1550 -963 385 -239 811 -504 947 -588 135 -85 249 -154 253 -154 4 0 4 17 0
38 -6 34 -411 1897 -776 3568 -87 402 -159 738 -159 747 0 13 649 563 2997
2542 258 217 261 220 230 227 -18 4 -1011 104 -2207 223 -1196 119 -2184 220
-2196 225 -15 6 -62 111 -199 446 -98 242 -412 1013 -697 1714 -285 701 -564
1388 -620 1525 -56 138 -104 253 -108 258 -3 4 -278 -610 -610 -1365z"
          />
        </g>
      </svg>
    </div>
  );
}
CardProduct.propTypes = {
  product: PropTypes.shape({
    pathImg: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
  }).isRequired,
};
export default CardProduct;
