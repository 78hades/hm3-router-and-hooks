import logoImg from "./icon-park-solid_game-ps.png";
import Loupe from "./Vector.png";
import Basket from "./Vector (1).png";
import Star from "./775819 (1).svg";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
 function Header() {
  const {countBasket} = useSelector(state => state.basket)
  const {countWishlist} = useSelector(state => state.wishlist)
  return (
    <>
      <header className="header">
        <Link className="path" to='/'>
        <div className="header__logo logo">
          <img className="logo__img" src={logoImg} alt="" />
          <div className="logo__text">
            <span className="logo__text-game">GAME</span>
            <span className="logo__text-store">STORE</span>
          </div>
        </div>
        </Link>
        <ul className="header__menu menu">
          <li className="menu__item"><a href="/">Shop</a></li>
          <li className="menu__item"><a href="/">Computer</a></li>
          <li className="menu__item"><a href="/">Game Headphones</a></li>
          <li className="menu__item"><a href="/">VR Glasses</a></li>
          <li className="menu__item"><a href="/">Keyboard</a></li>
          <li className="menu__item"><a href="/">Mouse Gaming</a></li>
        </ul>
        <div className="header__search">
          <img className="header__search-img" src={Loupe} alt="" />
          <input
            className="header__search-field"
            placeholder="Search"
            type="text"
          />
        </div>
        <div className="header__wishlist">
          <Link to='/basket'>
          <div className="header__wishlist-basket">
            <p>{countBasket}</p> <img src={Basket} alt="" />
          </div>
          </Link>
          <Link to='/wishlist'>
          <div className="header__wishlist-favorites">
            <img className="header__wishlist-favorites" src={Star} alt="" />
            <p>{countWishlist}</p>
          </div>
          </Link>
        </div>
      </header>
    </>
  );
}

export default Header